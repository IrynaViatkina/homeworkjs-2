let numFirst = +prompt("Please, enter first number: ", 0);
let numSecond = +prompt("Please, enter second number: ", 0);

while(isNaN(numFirst) || isNaN(numSecond) || (numFirst > numSecond) || (numFirst <= 1) || (numSecond <= 1) || (numFirst != parseInt(numFirst)) || (numSecond != parseInt(numSecond))){
	alert("Wrong data!");
	numFirst = +prompt("Please, enter first number: ", 0);
	numSecond = +prompt("Please, enter second number: ", 0);
}

let count = 0;
for(let i = numFirst; i <= numSecond; i++){
	if(isPrime(i)){
		console.log(i);
		count++;
	}
}

if(count == 0){
	alert("There are no prime numbers in this range.");
}

function isPrime(number){
	for(let i = 2; i <= Math.sqrt(number); i++){
		if(number % i == 0){
			return false;
		}
	}
	return true;
}