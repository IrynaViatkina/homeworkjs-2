let number = +prompt("Please, enter a number: ", 0);

for(let i = 1; i < number; i++){
	if(isPrime(i)){
		console.log(i);
	}
}

function isPrime(number){
	for(let i = 2; i <= Math.ceil(Math.sqrt(number)); i++){
		if(number % i == 0){
			return false;
		}
	}
	return true;
}